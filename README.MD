### dkr-14-gocalc


# 1) ������������  ����������� gocalc.

```bash
git clone git@gitlab.rebrainme.com:docker-course-students/gocalc.git
```


# 2)Dockerfile � Multi-stage build, � ������� ������ ����������� � ����� ������ (����� ��� ����� ����� golang:1.13.5-alpine), � ���������� � ������ ������ - alpine:3.10.3.

```bash
[root@vps gocalc]# cat Dockerfile
FROM golang:1.13.5-alpine AS builder

WORKDIR /go/src/

COPY main.go .

RUN apk --no-cache add git \
    && export GOPATH=/go/src/ \
    && go get -u github.com/prometheus/client_golang/prometheus \
    && go get -u github.com/prometheus/client_golang/prometheus/promhttp \
    && go get -u github.com/caarlos0/env \
    && go get -u github.com/lib/pq \
    && ls \
    && go build main.go

FROM alpine:latest

WORKDIR /go/src/

COPY --from=builder /go/src/ .

CMD ["./main"]

```

# 3)���� ����� ������

```bash
[root@vps gocalc]# docker build .
Sending build context to Docker daemon  91.14kB
Step 1/9 : FROM golang:1.13.5-alpine AS builder
 ---> 57ce7b9daa9b
Step 2/9 : WORKDIR /go/src/
 ---> Using cache
 ---> eb5ce0f75fc3
Step 3/9 : COPY main.go .
 ---> 52a8314177f3
Step 4/9 : RUN    apk --no-cache add git     && export GOPATH=/go/src/     && go get -u github.com/prometheus/client_golang/prometheus     && go get -u github.com/prometheus/client_golang/prometheus/promhttp     && go get -u github.com/caarlos0/env     && go get -u github.com/lib/pq     && ls     && go build main.go
 ---> Running in 5e02eb294f95
fetch http://dl-cdn.alpinelinux.org/alpine/v3.11/main/x86_64/APKINDEX.tar.gz
fetch http://dl-cdn.alpinelinux.org/alpine/v3.11/community/x86_64/APKINDEX.tar.gz
(1/5) Installing nghttp2-libs (1.40.0-r1)
(2/5) Installing libcurl (7.67.0-r1)
(3/5) Installing expat (2.2.9-r1)
(4/5) Installing pcre2 (10.34-r1)
(5/5) Installing git (2.24.3-r0)
Executing busybox-1.31.1-r8.trigger
OK: 22 MiB in 20 packages
main.go
pkg
src
Removing intermediate container 5e02eb294f95
 ---> ab6eb8efdeb6
Step 5/9 : FROM alpine:latest
 ---> d6e46aa2470d
Step 6/9 : WORKDIR /go/src/
 ---> Running in f7b7e1d79b87
Removing intermediate container f7b7e1d79b87
 ---> 53600f643b5f
Step 7/9 : RUN ls; pwd
 ---> Running in 3af2202ee89c
/go/src
Removing intermediate container 3af2202ee89c
 ---> 230689869910
Step 8/9 : COPY --from=builder /go/src/ .
 ---> c3411b04ba09
Step 9/9 : CMD ["./main"]
 ---> Running in b520f612953e
Removing intermediate container b520f612953e
 ---> c2394e4c0b8e
Successfully built c2394e4c0b8e

```

# 4)������ ������� �� ��������� ������ 

```bash
[root@vps gocalc]# docker images
REPOSITORY   TAG             IMAGE ID       CREATED          SIZE
<none>       <none>          c2394e4c0b8e   5 seconds ago    90MB
<none>       <none>          ab6eb8efdeb6   10 seconds ago   477MB
nginx        latest          bc9a0695f571   12 days ago      133MB
alpine       latest          d6e46aa2470d   6 weeks ago      5.57MB
golang       1.13.5-alpine   57ce7b9daa9b   11 months ago    359MB

```

# 5)history ���������� ������ .
```bash
[root@vps gocalc]# docker history c2394e4c0b8e
IMAGE          CREATED          CREATED BY                                      SIZE      COMMENT
c2394e4c0b8e   27 seconds ago   /bin/sh -c #(nop)  CMD ["./main"]               0B
c3411b04ba09   27 seconds ago   /bin/sh -c #(nop) COPY dir:51f0de9bcdf96c120�   84.5MB
230689869910   31 seconds ago   /bin/sh -c ls; pwd                              0B
53600f643b5f   31 seconds ago   /bin/sh -c #(nop) WORKDIR /go/src/              0B
d6e46aa2470d   6 weeks ago      /bin/sh -c #(nop)  CMD ["/bin/sh"]              0B
<missing>      6 weeks ago      /bin/sh -c #(nop) ADD file:f17f65714f703db90�   5.57MB

```

# 6)��������� ������������ ����������� � ���� ������� �� gitlab.rebrainme.com ��� ������ dkr-14-gocalc.

```bash
[root@vps gocalc]# git remote rename origin old-origin
[root@vps gocalc]# git remote add origin git@gitlab.rebrainme.com:docker_users_repos/1257/dkr-14-gocalc.git
[root@vps gocalc]# git push -u origin --all
Enumerating objects: 7, done.
Counting objects: 100% (7/7), done.
Compressing objects: 100% (6/6), done.
Writing objects: 100% (7/7), 18.00 KiB | 6.00 MiB/s, done.
Total 7 (delta 0), reused 3 (delta 0)
To gitlab.rebrainme.com:docker_users_repos/1257/dkr-14-gocalc.git
 * [new branch]      master -> master
Branch 'master' set up to track remote branch 'master' from 'origin'.
[root@vps gocalc]# git push -u origin --tags
Everything up-to-date
```
