FROM golang:1.13.5-alpine AS builder

WORKDIR /go/src/

COPY main.go .

RUN apk --no-cache add git \
    && export GOPATH=/go/src/ \
    && go get -u github.com/prometheus/client_golang/prometheus \
    && go get -u github.com/prometheus/client_golang/prometheus/promhttp \
    && go get -u github.com/caarlos0/env \
    && go get -u github.com/lib/pq \
    && ls \
    && go build main.go 

FROM alpine:latest  

WORKDIR /go/src/

COPY --from=builder /go/src/ .

CMD ["./main"]


